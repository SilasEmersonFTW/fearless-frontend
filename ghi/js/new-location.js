window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

        const stateResponse = await fetch(url);
        if(stateResponse.ok){
            const data = await stateResponse.json()

            const selectTag = document.getElementById('state');
            for (let state of data.states) {
              // Create an 'option' element
                const optionTag = document.createElement('option')
              // Set the '.value' property of the option element to the
              // state's abbreviation
                optionTag.value = state.abbreviation
              // Set the '.innerHTML' property of the option element to
              // the state's name
                optionTag.innerHTML = state.name
              // Append the option element as a child of the select tag
              selectTag.appendChild(optionTag)
            }
        }



    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));


        const locationUrl = 'http://localhost:8000/api/locations/';

        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
          formTag.reset();
          const newLocation = await locationResponse.json();
        }
        
    });

})
